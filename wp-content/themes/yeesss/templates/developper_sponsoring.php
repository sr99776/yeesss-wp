
<?php
/**
 * Template Name:Developper sponsoring locales
 */
get_header();
?>

<!--======== page-banner ========-->
<?php
$sponsoringbg = get_field('developper_sponsoring_banner');
if ($sponsoringbg): ?>
    <div class="page-banner"
         style="background-image: url('<?php echo esc_url($sponsoringbg['sponsoring_banner_bgimage']); ?>')">
        <div class="verticle-block">
            <div class="verticle-cell">
                <div class="container">
                    <span><?php echo $sponsoringbg['sponsoring_banner_title_1']; ?></span><br>
                    <span><?php echo $sponsoringbg['sponsoring_banner_title_2']; ?></span>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<!--======== close page-banner ========-->

<!--======== notre-constat ========-->
<?php
$votresituation = get_field('votre_situation');
if ($votresituation): ?>
    <div class="notre-constat">
        <div class="container">
            <div class="garantie-section">
                <h2><?php echo $votresituation['votre_sit_title']; ?></h2>
                <div class="title-borderbottom"></div>
            </div>
            <p class="garantie-paragraph"><?php echo $votresituation['votresit_sub_title']; ?></p>
        </div>
    </div>
<?php endif; ?>
<!--======== close notre-constat ========-->


<!--======== definissez-besoins ========-->
<?php
$notrevreponse = get_field('notrev_reponse_content');
if ($notrevreponse): ?>
<div class="definissez-besoins">
    <div class="container">
        <div class="row">
            <!-- col-md-6 -->
            <div class="col-md-6 mb-24">
                <div class="roundshadow-wrap">
                    <img src="<?php echo esc_url($notrevreponse['leftimage_notrev']); ?>" align="no-image" class="img-roundshadow">
                </div>
            </div>

            <!-- col-md-6 -->
            <div class="col-md-6">
                <div class="definissez-wrap">
                    <div class="garantie-section">
                        <h2><?php echo $notrevreponse['notre_reponse_title']; ?></h2>
                        <div class="title-borderbottom"></div>
                    </div>
                    <p><?php echo $notrevreponse['notre_reponse_paragraph']; ?></p>
                    <ol>
                        <?php
                        if( have_rows('notrev_reponse_content') ): while ( have_rows('notrev_reponse_content') ) : the_row();
                            if( have_rows('notre_reponse_list_content') ): while ( have_rows('notre_reponse_list_content') ) : the_row();?>
                                <li><?php echo get_sub_field('reponse_list_item');?></li>
                            <?php
                            endwhile; endif;
                        endwhile; endif;
                        ?>
                    </ol>
                </div>

                <!-- definissez-wrap -->
                <div class="definissez-wrap">
                    <div class="garantie-section">
                        <h2 class="nospace-right"><?php echo $notrevreponse['le_modele_economique']; ?></h2>
                        <div class="title-borderbottom"></div>
                    </div>
                    <ol>
                        <?php
                        if( have_rows('notrev_reponse_content') ): while ( have_rows('notrev_reponse_content') ) : the_row();
                            if( have_rows('le_modele_list_content') ): while ( have_rows('le_modele_list_content') ) : the_row();?>
                                <li><?php echo get_sub_field('le_modele_list_item');?></li>
                            <?php
                            endwhile; endif;
                        endwhile; endif;
                        ?>
                    </ol>
                </div>
            </div>

            <!-- button-col -->
            <div class="col-xs-12 text-center button-col">
                <?php
                $lemodelelink = $notrevreponse['le_modele_button'];
                if( $lemodelelink ):
                    $link_urlmo = $lemodelelink['url'];
                    $link_titlemo = $lemodelelink['title'];
                    ?>
                    <a class="btn btn-site ripple" href="<?php echo esc_url( $link_urlmo );?>?p=<?php echo $wp->request;?>"><?php echo esc_html( $link_titlemo ); ?></a>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>
<?php endif; ?>
<!--===== close definissez-besoins =====-->


<?php
// fetch footer file here
get_footer();
?>

