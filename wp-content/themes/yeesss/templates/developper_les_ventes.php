<?php
/**
 * Template Name:Développer les ventes
 */

// fetch header file here
get_header();

$data = get_field('recruter-developper-campagne');

?>

<!--======== page-banner ========-->
<div class="page-banner" style="background-image: url('<?php echo $data['background_image'] ?>')">
    <div class="verticle-block">
        <div class="verticle-cell">
            <div class="container">
                <span><?php echo $data['title']; ?></span>
                <span><?php echo $data['sub_title']; ?></span>
            </div>
        </div>
    </div>
</div>
<!--======== close page-banner ========-->

<!--======== notre-constat ========-->
<div class="notre-constat">
    <div class="container">
        <div class="garantie-section">
            <h2><?php echo $data['notre_constat']; ?></h2>
            <div class="title-borderbottom"></div>
        </div>

        <p class="garantie-paragraph"><?php echo $data['notre_constat_content']; ?></p>
    </div>
</div>
<!--======== close notre-constat ========-->

<!--======== definissez-besoins ========-->
<div class="definissez-besoins">
    <div class="container">
        <div class="row">
            <!-- col-md-6 -->
            <div class="col-md-6 mb-24">
                <div class="roundshadow-wrap">
                    <img src="<?php echo $data['notre_proposition_image']; ?>" align="no-image found"
                         class="img-roundshadow">
                </div>
            </div>

            <!-- col-md-6 -->
            <div class="col-md-6">
                <?php if (have_rows('recruter-developper-campagne')):
                    while (have_rows('recruter-developper-campagne')):
                        the_row();
                        ?>
                        <div class="definissez-wrap">
                            <?php
                            if (have_rows('content')):
                                while (have_rows('content')):
                                    the_row();
                                    $title = get_sub_field('title');
                                    ?>
                                    <div class="garantie-section">
                                        <h2><?php echo $title; ?></h2>
                                        <div class="title-borderbottom"></div>
                                    </div>
                                    <ol>
                                        <?php
                                        if (have_rows('de_valeur_content')):
                                            while (have_rows('de_valeur_content')):
                                                the_row();
                                                $steps = get_sub_field('de_valeur');
                                                ?>
                                                <li><?php echo $steps; ?></li>
                                            <?php endwhile;
                                        endif;
                                        ?>
                                    </ol>
                                <?php endwhile;
                            endif;
                            ?>
                        </div>
                    <?php
                    endwhile;
                endif; ?>
            </div>

            <!-- button-col -->
            <?php if (have_rows('recruter-developper-campagne')):
                while (have_rows('recruter-developper-campagne')):
                    the_row();
                    // Get sub field values.
                    if (have_rows('je_choisis_cette_offre_button')):
                        while (have_rows('je_choisis_cette_offre_button')):
                            the_row();
                            $title = get_sub_field('button_title');
                            $link = get_sub_field('link');
                            ?>
                            <div class="col-xs-12 text-center button-col">
                                <a href="<?php echo $link;?>?p=<?php echo $wp->request;?>"
                                   class="btn btn-site ripple"><?php echo $title; ?></a>
                            </div>
                        <?php
                        endwhile;
                    endif;
                endwhile;
            endif; ?>
        </div>
    </div>
</div>
<!--===== close definissez-besoins =====-->
<?php

// fetch footer file here
get_footer();
?>
