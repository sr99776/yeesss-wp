<?php
/**
 * Template Name: Developper valeur clients
 */

get_header();
?>


<!--======== page-banner ========-->
<?php
$valeurbanner = get_field('developper_valeur_banner');
if ($valeurbanner): ?>
    <div class="page-banner bg-campagne"
         style="background-image: url('<?php echo esc_url($valeurbanner['developper_banner_bgimage']); ?>')">
        <div class="verticle-block">
            <div class="verticle-cell">
                <div class="container">
                    <span><?php echo $valeurbanner['valeur_banner_title_1']; ?></span><br>
                    <span><?php echo $valeurbanner['valeur_banner_title_2']; ?></span>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<!--======== close page-banner ========-->

<!--======== notre-constat ========-->
<?php
$developperconstat = get_field('developper_notre_constat');
if ($developperconstat): ?>
    <div class="notre-constat">
        <div class="container">
            <div class="garantie-section">
                <h2><?php echo $developperconstat['notreconstat_title']; ?></h2>
                <div class="title-borderbottom"></div>
            </div>
            <p class="garantie-paragraph"><?php echo $developperconstat['notreconstat_sub_title']; ?></p>
        </div>
    </div>
<?php endif; ?>
<!--======== close notre-constat ========-->

<!--======== definissez-besoins ========-->
<?php
$valeurproposition = get_field('developper_valeur_proposition');
if ($valeurproposition): ?>
    <div class="definissez-besoins">
        <div class="container">
            <div class="row">
                <!-- col-md-6 -->
                <div class="col-md-6 mb-24">
                    <div class="roundshadow-wrap">
                        <img src="<?php echo esc_url($valeurproposition['leftimage_valeur_proposition']); ?>" align="no-image" class="img-roundshadow">
                    </div>
                </div>

                <!-- col-md-6 -->
                <div class="col-md-6">
                    <div class="definissez-wrap">
                        <div class="garantie-section">
                            <h2><?php echo $valeurproposition['valeur_proposition_title']; ?></h2>
                            <div class="title-borderbottom"></div>
                        </div>

                        <ol>
                            <?php
                            if( have_rows('developper_valeur_proposition') ): while ( have_rows('developper_valeur_proposition') ) : the_row();
                                if( have_rows('notre_proposition_list_content') ): while ( have_rows('notre_proposition_list_content') ) : the_row();?>
                                    <li><?php echo get_sub_field('proposition_list_item');?></li>
                                <?php
                                endwhile; endif;
                            endwhile; endif;
                            ?>
                        </ol>
                    </div>
                </div>

                <!-- button-col -->
                <div class="col-xs-12 text-center button-col">
                    <?php
                    $choisislink = $valeurproposition['valeur_choisis_button'];
                    if( $choisislink ):
                        $link_url = $choisislink['url'];
                        $link_title = $choisislink['title'];
                        ?>
                        <a class="btn btn-site ripple" href="<?php echo esc_url( $link_url );?>?p=<?php echo $wp->request;?>"><?php echo esc_html( $link_title ); ?></a>
                    <?php endif; ?>
                </div>




            </div>
        </div>
    </div>
<?php endif; ?>
<!--===== close definissez-besoins =====-->


<?php

// fetch footer file here
get_footer();
?>

