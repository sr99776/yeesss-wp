<?php
/**
 * Created by PhpStorm.
 * User: Raan India
 * Date: 1/20/2020
 * Time: 10:53 AM
 */

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Settings',
        'menu_title' => 'Settings',
        'menu_slug' => 'theme-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Haut de page',
        'menu_title' => 'Haut de page',
        'parent_slug' => 'theme-settings',
    ));

}


//************CALL CONTACT FORM 7 SUCCESS POPUP****************//

add_action( 'wp_footer', 'mycustom_wp_footer' );
function mycustom_wp_footer() {
?>
    <script type="text/javascript">
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            if ( '49' === event.detail.contactFormId ) { // Change 123 to the ID of the form
                jQuery('#modalite-form').modal('show'); //this is the bootstrap modal popup id
            }

            if ( '61' === event.detail.contactFormId ) { // Change 123 to the ID of the form
                jQuery('#suis-eligible').modal('show'); //this is the bootstrap modal popup id
            }
        }, false );
    </script>
<?php
}

//************END CALL CONTACT FORM 7 SUCCESS POPUP****************//
