<?php
/**
 * Template Name:Accueil
 */

// fetch header file here
get_header();

?>


<div class="notre-constat court-terme">
    <div class="container">
        <div class="garantie-section">
            <h2><?php _e( 'Oops! That page can&rsquo;t be found.', 'yeesss' ); ?></h2>
            <div class="title-borderbottom"></div>
        </div>
    </div>
</div>

<?php

// fetch footer file here
get_footer();
?>
