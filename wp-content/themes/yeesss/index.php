<?php
/**
 * Template Name:Accueil
 */

// fetch header file here
get_header();

$home = get_field('');
?>

<?php $topContent = get_field('nos_dispositifs_pour'); ?>
<!--======== page-banner ========-->
<div class="page-banner home-banner" style="background-image: url('<?php echo $topContent['background_image']; ?>')">
    <div class="verticle-block dispositifs-home">
        <div class="verticle-cell">
            <div class="container">
                <span><?php echo $topContent['nos_dispositifs_title']; ?></span><br>
                <span><?php echo $topContent['developper_votre_title']; ?></span>
            </div>

            <div class="container-span">
                <span><?php echo $topContent['pour_que_vos_title']; ?></span><br>
                <span><?php echo $topContent['pour_que_vos_title_sub']; ?></span>
            </div>

            <div class="text-center button-col">
                <a href="javascript:;" class="btn btn-site ripple"><?php echo $topContent['button_name']; ?></a>
            </div>
        </div>
    </div>
</div>
<!--======== close page-banner ========-->

<?php $middleContent = get_field('decouvrez_nos_dernieres_experiences'); ?>
<!--======== notre-experiences ========-->
<div class="notre-constat marginBottom">
    <div class="container">
        <div class="garantie-section">
            <h2><?php echo $middleContent['title']; ?></h2>
            <div class="title-borderbottom"></div>
        </div>

        <!--        dernieres-img-->
        <div class="row dernieres-row">
            <?php if (have_rows('decouvrez_nos_dernieres_experiences')):
                while (have_rows('decouvrez_nos_dernieres_experiences')):
                    the_row();
                    if (have_rows('dernieres_experiences')):
                        while (have_rows('dernieres_experiences')):
                            the_row();
                            $image = get_sub_field('image');
                            $title = get_sub_field('title');
                            $content = get_sub_field('content');
                            $buttonName = get_sub_field('button');
                            $link = get_sub_field('link');
                            ?>
                            <div class="col-md-4 col-xs-6">
                                <div class="dernieres-img">
                                    <img src="<?php echo $image; ?>" alt="" class="img-responsive">

                                    <div class="dernieres-hover">
                                        <h3><?php echo $title; ?></h3>
                                        <p><?php echo $content; ?></p>
                                        <div class="dernieres-wrap">
                                            <?php if($link): ?>
                                                <a href="<?php echo $link; ?>" class="btn btn-dernieres "><?php echo $buttonName; ?></a>
                                            <?php else : ?>
                                                <a href="javascript:void(0);" class="btn btn-dernieres up-coming">À venir</a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        endwhile;
                    endif;
                endwhile;
            endif;
            ?>
        </div>
    </div>
</div>
<!--======== close notre-experiences ========-->

<?php $businessContent = get_field('brand_business_relation'); ?>
<!--======== des-dispositifs ========-->
<div class="notre-constat des-dispositifs">
    <div class="container">
        <div class="garantie-section">
            <p><?php echo $businessContent['title']; ?></p>
            <h2><?php echo $businessContent['sub_title']; ?></h2>
            <div class="title-borderbottom"></div>
        </div>

        <div class="row">
            <?php if (have_rows('brand_business_relation')):
                while (have_rows('brand_business_relation')):
                    the_row();
                    if (have_rows('business_relation')):
                        while (have_rows('business_relation')):
                            the_row();
                            $icon = get_sub_field('icon');
                            $title = get_sub_field('title');
                            $content = get_sub_field('content');
                            ?>
                            <div class="col-md-3">
                                <div class="brand-business">
                                    <img src="<?php echo $icon; ?>" alt=""
                                         class="img-respnsive">
                                    <h3><?php echo $title; ?></h3>
                                    <p><?php echo $content; ?></p>
                                </div>
                            </div>
                        <?php
                        endwhile;
                    endif;
                endwhile;
            endif;
            ?>
        </div>
    </div>
</div>
<!--===== close des-dispositifs =====-->

<?php $bottomContent = get_field('agence_en_marketing_relationnel'); ?>
<!--======== recherche-footer ========-->
<div class="page-banner page-recherche"
     style="background-image: url('<?php echo $bottomContent['background_image']; ?>')">
    <div class="verticle-block">
        <div class="verticle-cell">
            <div class="container-span">
                <?php echo $bottomContent['top_title']; ?>
            </div>

            <div class="container">
                <span><?php echo $bottomContent['title']; ?></span><br>
                <span><?php echo $bottomContent['sub_title']; ?></span>
            </div>

            <div class="text-center button-col">
                <a href="javascript:;" class="btn btn-site ripple"><?php echo $bottomContent['button']; ?></a>
            </div>
        </div>
    </div>
</div>
<!--======== close recherche-footer ========-->

<?php

// fetch footer file here
get_footer();
?>

