<?php
/**
 * Template Name: site-offers
 */
get_header();
$dataUrl =
$data = get_field('nos_5_offres');
?>


<!--======== page-banner ========-->
<div class="page-banner contact-bg" style="background-image: url('<?php echo $data['backgrpund_image'] ?>')">
    <div class="verticle-block">
        <div class="verticle-cell">
            <div class="container">
                <span><?php echo $data['title']; ?></span><br>
                <span><?php echo $data['sub_title']; ?></span>
            </div>
        </div>
    </div>
</div>
<!--======== close page-banner ========-->
<!--====== garantie ======-->
<div class="site-offersAll-bg">
    <div class="garantie-contact">
        <div class="container">
            <div class="garantie-section  text-center ">
                <span><?php echo $data ['notre_approche']; ?></span>
                <h2><?php echo $data ['proposer_notre']; ?></h2>
                <div class="title-borderbottom"></div>
            </div>
        </div>
    </div>


    <!-- offres-performance-->
    <div class="offres-performance">
        <div class="container">
            <div class="row">

                <?php
                if (have_rows('new_customers')) {
                    while (have_rows('new_customers')) {
                        the_row();
                        $customers_images = get_sub_field('customers_images');
                        $customers_title = get_sub_field('customers_title');
                        $customers_link = get_sub_field('customers_link');
                        ?>

                        <div class="col-sm-6 col-md-4">
                            <a href="<?php echo $customers_link; ?>">
                                <div class="thumbnail">
                                    <div class="page-banner"
                                         style="background-image: url('<?php echo $customers_images ?>')"></div>
                                    <div class="caption">
                                        <h3><?php echo $customers_title; ?></h3>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>


<?php

// fetch footer file here
get_footer();
?>
