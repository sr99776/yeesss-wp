<?php
/**
 * Template Name:Suis-éligible a l’offre
 */

// fetch header file here
get_header();
$urlName = $_GET['p'];
$data = get_field('nos_dispositifs_pour_developper');

?>

<!--======== page-banner ========-->
<div class="page-banner contact-bg" style="background-image: url('<?php echo $data['background_image']; ?>')">
    <div class="verticle-block">
        <div class="verticle-cell">
            <div class="container">
                <span><?php echo $data['title']; ?></span><br>
                <span><?php echo $data['sub_title']; ?></span>
            </div>
        </div>
    </div>
</div>
<!--======== close page-banner ========-->
<!--====== garantie ======-->
<div class="garantie-contact">
    <div class="container">
        <div class="garantie-section  text-center ">
            <div class="brief-1">
                <span><?php echo $data['suis-eligible_a_l’offre_title']; ?></span>
                <h2><?php echo $data['suis-eligible_a_l’offre_sub_title']; ?></h2>
            </div>

            <div class="brief-2">
                <span><?php echo $data['modalite_title']; ?></span>
                <h2><?php echo $data['modalite_sub_title']; ?></h2>
            </div>

            <div class="title-borderbottom"></div>
        </div>
    </div>
</div>

<div class="contact-background">
    <div class="container">
        <div class="row">
            <?php
            // Suis-éligible a l’offre Form Contact form 7
            if ($urlName == "recruter") {
                echo do_shortcode('[contact-form-7 id="61" title="Suis-éligible a l’offre Form"]');
            } elseif ($urlName == "developper-sponsoring-locales") {
                echo do_shortcode('[contact-form-7 id="312" title="Developper sponsoring locales Form"]');
            } elseif ($urlName == "developper-les-ventes") {
                echo do_shortcode('[contact-form-7 id="313" title="Développer les ventes Form"]');
            } elseif ($urlName == "developper-valeur-clients") {
                echo do_shortcode('[contact-form-7 id="314" title="Developper valeur clients Form"]');
            } else {
                echo do_shortcode('[contact-form-7 id="61" title="Suis-éligible a l’offre Form"]');
            }
            ?>
        </div>
    </div>
</div>

<?php

// fetch footer file here
get_footer();
?>
