<?php
/**
 * Template Name: Modalité
 */

// fetch header file here
get_header();

$data = get_field('nos_dispositifs_pour_developper');
?>

<!--======== page-banner ========-->
<div class="page-banner contact-bg" style="background-image: url('<?php echo $data['background_image']; ?>')">
    <div class="verticle-block">
        <div class="verticle-cell">
            <div class="container">
                <span><?php echo $data['title']; ?></span><br>
                <span><?php echo $data['sub_title']; ?></span>
            </div>
        </div>
    </div>
</div>
<!--======== close page-banner ========-->
<!--====== garantie ======-->
<div class="garantie-contact">
    <div class="container">
        <div class="garantie-section  text-center ">
            <span><?php echo $data['middle_title']; ?></span>
            <h2><?php echo $data['middle_sub_title']; ?></h2>
            <div class="title-borderbottom"></div>
        </div>
    </div>
</div>

<div class="contact-background">
    <div class="container">
        <div class="row">
            <?php
            // Modalite Form Contact form 7
                echo do_shortcode('[contact-form-7 id="49" title="Modalité Form"]');
            ?>
        </div>
    </div>
</div>

<?php

// fetch footer file here
get_footer();
?>
