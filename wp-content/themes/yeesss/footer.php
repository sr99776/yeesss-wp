<?php
/**
 * Footer file for YEESSS
 */

?>

<!--=== bottom-bg ===-->
<div class="bottom-bg"></div>


<!--========== Javascript attachment files ==========-->
<!--  bootstrap script -->
<script type="text/javascript" src="<?php bloginfo('template_url');?>/vendor/javascripts/bootstrap.min.js"></script>
<!-- custom javascript -->
<script type="text/javascript" src="<?php bloginfo('template_url');?>/vendor/js/custom.js"></script>
<!--======= Close Javascript attachment files =======-->


</body>
</html>
