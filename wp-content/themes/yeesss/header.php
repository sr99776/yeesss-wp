<?php
/**
 * Header file of YEESSS
 */

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>YEESSS</title>
    <!-- meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- meta description & keywords -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <!-- favicon icon -->
    <link rel='icon' type='image/x-icon' href="<?php bloginfo('template_directory');?>/images/favicon.png"/>
    <!-- fontawesom cdn -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- jquery library -->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/vendor/javascripts/jquery.min.js"></script>
    <!-- css links -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/dist/style.css"/>

</head>
<body>
<?php $header = get_field('haut_de_page','option'); ?>
<!--======== header-area ========-->
<header class="header-area">
    <!-- header-contact -->
    <div class="header-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="head-contact">
                        <ul>
                            <li><?php echo $header['address']; ?></li>
                            <li><a href="tel:<?php echo $header['tel']; ?>"><i class="fa fa-phone"></i><?php echo $header['tel']; ?></a></li>
                            <li><a href="mailto:<?php echo $header['email']; ?>"><i class="fa fa-envelope"></i><?php echo $header['email']; ?></a></li>
                            <li><a href="<?php echo $header['offres_link']; ?>"><?php echo $header['offres']; ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- header-wrapper -->
    <div class="header-wrapper">
        <div class="nav">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="header-wrap">
                            <div class="logo-top">
                                <a href="<?php echo get_site_url(); ?>" title="">
                                    <img src="<?php echo $header['logo']; ?>" alt="L'agence Yeesss"
                                         data-retina="http://yeesss.fr/wp-content/uploads/2019/05/logo-yeesss-1.jpg">
                                </a>
                            </div>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>

        <!--<nav class="navbar navbar-yeesss">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="header-wrap">
                    <div class="logo-top">
                        <a href="#" title="">
                            <img src="http://yeesss.sandboxconcept.com/wp-content/uploads/2020/01/logo-header.png" alt="L'agence Yeesss" data-retina="http://yeesss.fr/wp-content/uploads/2019/05/logo-yeesss-1.jpg">
                        </a>
                    </div>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Qui Sommes-nous?</a></li>
                    <li><a href="#">Promesse</a></li>
                    <li><a href="#">ExpertiseS</a></li>
                    <li><a href="#">Expériences</a></li>
                    <li><a href="#">Associés</a></li>
                    <li><a href="#">Contact</a></li>
                    <li class="search-section"></li>
                </ul>
            </div>
        </nav>-->
    </div>


</header>
<!--===== close header-area =====-->
