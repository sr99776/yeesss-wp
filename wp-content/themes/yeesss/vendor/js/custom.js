// -------- Jquery start ------- //
$(function () {


    // type="tel"
    $('.contact-background [type="tel"]').bind('keydown', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        console.log(charCode);
        if ((charCode > 47 && charCode < 58) || (charCode > 95 && charCode < 106) || charCode == 8 || (charCode > 36 && charCode < 41)) {
            return true;
        } else {
            return false;
        }
    });

    // votre-brief
    $('.votre-brief').click(function (event) {

        // brief-1
        $('.brief-1 .form-control').each(function () {
            if ($(this).val() == '' || $(this).val() === '') {
                $(this).next('.wpcf7-not-valid-tip').remove();
                $(this).after('<span role="alert" class="wpcf7-not-valid-tip">Champs requis</span>');
            } else if ($(this).val() != '') {
                $(this).next('.wpcf7-not-valid-tip').remove();
            }
        });

        if($('.brief-1 .wpcf7-not-valid-tip').length == 0){
            $('.brief-1').slideUp(300);
            $('.brief-2').slideDown(300);
        }
    });

    // ripple btn effect
    $('.ripple').click(function (event) {
        var $btn = $(this),
            $div = $('<div/>'),
            btnOffset = $btn.offset(),
            xPos = event.pageX - btnOffset.left,
            yPos = event.pageY - btnOffset.top;

        $div.addClass('ripple-effect');
        $div
            .css({
                height: $btn.height(),
                width: $btn.height(),
                top: yPos - ($div.height() / 2),
                left: xPos - ($div.width() / 2),
                background: $btn.data("ripple-color") || "rgba(0, 0, 0, 0.05)"
            });
        $btn.append($div);

        window.setTimeout(function () {
            $div.fadeOut('fast');
        }, 600);
    });

    // equalheight
    equalheight = function (container) {
        var currentTallest = 0,
            currentRowStart = 0,
            rowDivs = new Array(),
            $el,
            topPosition = 0;
        $(container).each(function () {

            $el = $(this);
            $($el).height('auto')
            topPostion = $el.position().top;

            if (currentRowStart != topPostion) {
                for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs.length = 0; // empty the array
                currentRowStart = topPostion;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
            }
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    }

    // dispositifs-home
    $('.dispositifs-home').css('height', $(window).height() - $('.header-area').innerHeight());

    $(window).load(function () {
        equalheight('.site-offersAll-bg .caption');
        equalheight('.dernieres-hover');
        $('.dispositifs-home').css('height', $(window).height() - $('.header-area').innerHeight());
    });

    $(window).resize(function () {
        equalheight('.site-offersAll-bg .caption');
        equalheight('.dernieres-hover');

        // dispositifs-home
        $('.dispositifs-home').css('height', $(window).height() - $('.header-area').innerHeight());
    });

//------------------Form validations-------------------//

    $("#submit-btn-modalite").on("click", function (e) {

        // remove errors
        $('.form-modalite').remove();

        var telephone = $('#telephone').val();
        var email = $('input[name="email"]').val();
        var nom = $('input[name="nom"]').val();
        var prenom = $('input[name="prenom"]').val();
        var societe = $('input[name="societe"]').val();
        var souhaite = $("#souhaite option:selected").val();

        var err = false;

        // validate telephone
        if (telephone !== "") {
            $("#telephone").parent().removeClass("error-custom-modalite");
        } else {
            $("#telephone").parent().addClass("error-custom-modalite");
        }
        // validate email
        if (email !== "") {
            $("#email").parent().removeClass("error-custom-modalite");
            // check email validate
            if (validateEmail(email)) {
                $("#email").parent().removeClass("error-email-ok");
            } else {
                $("#email").parent().addClass("error-email-ok");
            }
        } else {

            $("#email").parent().addClass("error-custom-modalite");
        }
        // validate nom
        if (nom !== "") {
            $("#nom").parent().removeClass("error-custom-modalite");
        } else {
            $("#nom").parent().addClass("error-custom-modalite");
        }
        // validate prenom
        if (prenom !== "") {
            $("#prenom").parent().removeClass("error-custom-modalite");
        } else {
            $("#prenom").parent().addClass("error-custom-modalite");
        }
        // validate societe
        if (societe !== "") {
            $("#societe").parent().removeClass("error-custom-modalite");
        } else {
            $("#societe").parent().addClass("error-custom-modalite");
        }
        // validate souhaite
        if (souhaite !== "") {
            $("#souhaite").parent().removeClass("error-custom-modalite");
        } else {
            $("#souhaite").parent().addClass("error-custom-modalite");
        }


        // check error custom offre class
        if ($('.wpcf7-form-control-wrap').hasClass("error-custom-modalite")) {
            err = true;
            // append error
            $(".error-custom-modalite").append('<span role="alert" class="wpcf7-not-valid-tip form-modalite">Champs requis</span>');
        }

        // check email error
        if ($('.wpcf7-form-control-wrap').hasClass("error-email-ok")) {
            err = true;
            // append error
            $(".error-email-ok").append('<span role="alert" class="wpcf7-not-valid-tip form-modalite">Email invalide</span>');
        }

        // check telephone error
        if ($('.wpcf7-form-control-wrap').hasClass("error-tel-ok")) {
            err = true;
            // append error
            $(".error-tel-ok").append('<span role="alert" class="wpcf7-not-valid-tip form-modalite tel-error">Numéro invalide</span>');
        }


        // if has error
        if (err === true) {
            // stop form to load
            e.preventDefault();
        }


    });

    // validate Email
    function validateEmail(a) {
        var b = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return b.test(String(a).toLowerCase())
    }

    // tel-phone validation by key event
    $(".tel-phone").on("keyup", function () {

        // remove old telephone error
        $(this).parents('.wpcf7-form-control-wrap').find('.wpcf7-not-valid-tip').remove();

        var pattern = /[0]{1}[0-9]{9}/g;
        var tel = $(this).val();

        // hold on 10 digits only
        if (tel.length > 10) {
            $(this).val(tel.substring(0, 10));
        }

        // check regex here
        var result = pattern.test(tel);

        if (result.toString() === "true" || tel.length === 0) {
            $(this).parent().removeClass('error-tel-ok');

        } else {
            $(this).parent().addClass('error-tel-ok');
        }

        // length message
        if ($(this).parent().hasClass('error-tel-ok')) {
            $(".error-tel-ok").append('<span role="alert" class="wpcf7-not-valid-tip form-modalite tel-error">Entrez un numéro à 10 chiffres.</span>');
        }
    });


});


