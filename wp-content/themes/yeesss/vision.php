<?php
/**
 * Template Name: vision
 */
get_header();

$data = get_field('notre_vision');
?>

<!--======== page-banner ========-->
<div class="page-banner contact-bg" style="background-image: url('<?php echo $data['vision_background'] ?>')">
    <div class="verticle-block">
        <div class="verticle-cell">
            <div class="container">
                <span><?php echo $data['title_vision']; ?></span><br>
                <span><?php echo $data['sub_title_vision']; ?></span>
            </div>
        </div>
    </div>
</div>
<!--======== close page-banner ========-->

<!--====== garantie ======-->
<div class="garantie-contact">
    <div class="container">
        <div class="garantie-section text-center">
            <span><?php echo $data['vision_notre']; ?></span>
            <h2><?php echo $data['risque_content']; ?></h2>
            <div class="title-borderbottom"></div>
        </div>
    </div>
</div>
<!--==== close garantie ====-->

<!--======== definissez-besoins ========-->

<?php
$data = get_field('create_an_offer_that');
?>
<div class="definissez-besoins createvision">
    <div class="container">
        <div class="row">
            <!-- col-md-6 -->
            <div class="col-md-6 col-xs-12 mb-24 pull-right">
                <div class="roundshadow-wrap">
                    <img src="<?php echo $data['right_offers_images']; ?>" align="no-image found"
                         class="img-roundshadow">
                </div>
            </div>

            <!-- col-md-6 -->
            <div class="col-md-6 col-xs-12">
                <div class="definissez-wrap">
                    <div class="garantie-section">
                        <span class="sm-heading"><?php echo $data['left_notre_top_title']; ?></span>
                        <h2><?php echo $data['create_an_offer_title']; ?></h2>
                        <div class="title-borderbottom"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--===== close definissez-besoins =====-->


<!--======== court-terme ========-->
<?php
$data = get_field('nos_constats_vision');
?>
<div class="notre-constat court-terme">
    <div class="container">
        <div class="garantie-section">
            <h2><?php echo $data['nos_constats_heading_vision']; ?></h2>
            <div class="title-borderbottom"></div>
        </div>

        <div class="row">

            <?php
            if (have_rows('nos_constats_vision')) {
                while (have_rows('nos_constats_vision')) {
                    the_row();
                    if (have_rows('des_activations')) {
                        while (have_rows('des_activations')) {
                            the_row();
                            $title_des_activations = get_sub_field('title_des_activations');
                            $commercial_bottom_content_ = get_sub_field('commercial_bottom_content_');
                            ?>
                            <div class="col-sm-4 col-xs-12">
                                <div class="court-col">
                                    <p class="garantie-paragraph"><?php echo $title_des_activations; ?></p>
                                    <span class="sm-gray-court"><?php echo $commercial_bottom_content_; ?></span>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
            }
            ?>

        </div>
    </div>
</div>
<!--======== close court-terme ========-->

<!--======== definissez-besoins ========-->

<?php
$data = get_field('investissement_vision');
?>
<div class="definissez-besoins">
    <div class="container">
        <div class="row">
            <!-- col-md-6 -->
            <div class="col-md-6 mb-24">
                <div class="roundshadow-wrap">
                    <img src="<?php echo $data['votre_attente_left_image'] ?>" align="no-image found"
                         class="img-roundshadow">
                </div>
            </div>

            <!-- col-md-6 -->
            <div class="col-md-6">
                <div class="definissez-wrap">
                    <div class="garantie-section">
                        <span class="sm-heading"><?php echo $data['votre_attente_top_title']; ?></span>
                        <h2><?php echo $data['votre_attente_big_contact']; ?></h2>
                        <div class="title-borderbottom"></div>
                    </div>

                    <ol class="garantie-list">
                        <?php
                        if( have_rows('investissement_vision') ): while ( have_rows('investissement_vision') ) : the_row();
                            if( have_rows('votre_attente_list') ): while ( have_rows('votre_attente_list') ) : the_row();?>
                                <li><?php echo get_sub_field('votre_attente_item');?></li>
                            <?php
                            endwhile; endif;
                        endwhile; endif;
                        ?>
                    </ol>

                </div>
            </div>

        </div>
    </div>
</div>
<!--===== close definissez-besoins =====-->


<!--======== page-banner ========-->
<?php
$sponsoringbg = get_field('decouvrez_section');
if ($sponsoringbg): ?>
<div class="page-banner decouvrez-banner" style="background-image: url('<?php echo $sponsoringbg['decouvrez_background']; ?>')">
    <div class="verticle-block">
        <div class="verticle-cell">
            <div class="container text-center button-col">
                <a href="<?php echo $sponsoringbg['decouvrez_button_vision']['url']; ?>" class="btn btn-site ripple"><?php echo $sponsoringbg['decouvrez_button_vision']['title']; ?></a>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<!--======== close page-banner ========-->


<?php

// fetch footer file here
get_footer();
?>
